﻿using System.Text.Json;

namespace Otus.FileGetterService
{
    public class MyBackgroundService : IHostedService
    {
        public Task StartAsync(CancellationToken cancellationToken)
        {
            DoEvery5Seconds(cancellationToken);
            return Task.CompletedTask;
        }

        private void TryRead()
        {
            var path = $"C:\\otus\\file-{DateTime.Now:yyyy-MM-dd}.json";
            if (File.Exists(path))
            {
                Console.WriteLine($"Начинаю читать {path}");
                var s = File.ReadAllText(path);
                var users = JsonSerializer.Deserialize<User[]>(s);
                foreach (var u in users!)
                {
                    Console.WriteLine($"получил [{u.Id}] '{u.Login}'");
                }
                File.Delete(path);
            }
        }
        private async Task DoEvery5Seconds(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    
                    TryRead();
                }
                catch (Exception ex)
                {
                    // обработка ошибки однократного неуспешного выполнения фоновой задачи
                }

                await Task.Delay(5000, stoppingToken);
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
